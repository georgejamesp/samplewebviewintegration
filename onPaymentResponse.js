
const state = {
    registeredCallbacks : {}
}

// To be called ahead of initiate / process calls
exports.registeredCallback = function (requestId, callback) {
    state.registeredCallbacks[requestId] = callback;
}

window.onPaymentResponse = function (stringResponse) {
    var payload = JSON.parse(stringResponse);
    var event = payload.event;
    if (event == "show_loader") {
        // Show some loader here
    } else if (event == "hide_loader") {
        // Hide Loader
    } else if (event == "initiate_result") {
        var requestId = payload.requestId || payload.payload.requestId
        state.registeredCallbacks[requestId](payload)
        // Get the response
    } else if (event == "process_result") {
        var requestId = payload.requestId || payload.payload.requestId
        state.registeredCallbacks[requestId](payload)
        // Get the response
    }
}