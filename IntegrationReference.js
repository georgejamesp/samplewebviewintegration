var initiate = function (payload) {
    if(!HyperServices.isInitialised()) {
        HyperServices.initiate(JSON.stringify(payload));
        return;
    }
    console.log("Already initialised");
}

var process = function (payload) {
    if(HyperServices.isInitialised()) {
        HyperServices.process(JSON.stringify(payload));
        return;
    } else {
        // Retrigger initiate call the process function
    }
}

var terminate = function () {
    HyperServices.terminate();
}

var preFetch = function (payload) {
    HyperServices.preFetch(JSON.stringify(payload));
}