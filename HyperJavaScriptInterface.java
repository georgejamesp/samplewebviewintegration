package in.juspay.devtools;

import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import in.juspay.hypersdk.data.JuspayResponseHandler;
import in.juspay.hypersdk.ui.HyperPaymentsCallbackAdapter;
import in.juspay.services.HyperServices;

public class HyperJavaScriptInterface extends HyperPaymentsCallbackAdapter {

    private final FragmentActivity activity;
    private final WebView webView;
    private final HyperServices hyperService;

    public HyperJavaScriptInterface(@NonNull FragmentActivity activity, @NonNull ViewGroup parent, @NonNull WebView webView) {
        this.activity = activity;
        this.webView = webView;
        this.hyperService = new HyperServices(activity, parent);
        webView.addJavascriptInterface(this, "HyperServices");
    }

    @JavascriptInterface
    public boolean initiate (String StringPayload) {
        JSONObject payload = null;
        try {
            payload = new JSONObject(StringPayload);
            hyperService.initiate(payload, this);
            return true;
        } catch (JSONException e) {
        }
        // Returns false on failure
        return false;
    }

    @JavascriptInterface
    public boolean process (String StringPayload) {
        if (!hyperService.isInitialised()) return false;
        JSONObject payload = null;
        try {
            payload = new JSONObject(StringPayload);
            hyperService.process(payload);
            return true;
        } catch (JSONException e) {
        }
        // Returns false on failure
        return false;
    }

    @JavascriptInterface
    public boolean preFetch(String StringPayload) {
        JSONObject payload = null;
        try {
            payload = new JSONObject(StringPayload);
            HyperServices.preFetch(activity, payload);
            return true;
        } catch (JSONException e) {
        }
        // Returns false on failure
        return false;
    }

    @JavascriptInterface
    public void terminate() {
        hyperService.terminate();
    }

    @JavascriptInterface
    public boolean isInitialised () {
        return hyperService.isInitialised();
    }

    @Override
    public void onEvent(JSONObject jsonObject, JuspayResponseHandler juspayResponseHandler) {
        String command;
        try {
            command = String.format("window.onPaymentResponse(decodeURIComponent('%s'));", URLEncoder.encode(jsonObject.toString(), "UTF-8").replace("+", "%20"));
        } catch (UnsupportedEncodingException e) {
            // If in some rare case UTF-8 is not supported on some version of java; This is very unlikely, but since code is critical having old code as a fallback;
            command = String.format("window.onPaymentResponse('%s');", "Failure");
        }
        webView.evaluateJavascript(command,null);
    }
}
