//
//  SampleViewController.swift
//
//  Copyright © Juspay Technologies. All rights reserved.
//

import UIKit
import WebKit

class SampleViewController: UIViewController {

    var webView: WKWebView!
    
    // Step 2
    var hyperServices: HyperJSInterface!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let indexURL = Bundle.main.url(forResource: "index", withExtension: "html")
            
        self.webView = WKWebView(frame: self.view.bounds)
        
        // Step 3
        self.webView.navigationDelegate = self
        self.webView.uiDelegate = self
        
        self.webView.loadFileURL(indexURL!, allowingReadAccessTo: indexURL!)
        
        // Step 4
        self.hyperServices = HyperJSInterface(baseViewController: self, webView: webView)
        
        self.view.addSubview(self.webView)
    }
}

extension SampleViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        // Step 4
        self.hyperServices.addHyperServicesInterface()
    }
}

extension SampleViewController: WKUIDelegate {
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        
        // Step 5
        if (prompt.range(of: "methodName") != nil) {
            self.hyperServices.webView(webView, runJavaScriptTextInputPanelWithPrompt: prompt, defaultText: defaultText, initiatedByFrame: frame, completionHandler: completionHandler)
        } else {
            // Do something else
        }
    }
}
