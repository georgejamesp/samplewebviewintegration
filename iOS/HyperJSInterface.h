//
//  HyperJSInterface.h
//
//  Copyright © Juspay Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WKWebView.h>

NS_ASSUME_NONNULL_BEGIN

@interface HyperJSInterface : NSObject

@property (nonatomic, weak) WKWebView *webView;
@property (nonatomic, weak) UIViewController *baseViewController;

- (id)initWithBaseViewController:(UIViewController *)viewController webView:(WKWebView *)webView;
- (void)addHyperServicesInterface;
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(nonnull NSString *)prompt defaultText:(nullable NSString *)defaultText initiatedByFrame:(nonnull WKFrameInfo *)frame completionHandler:(nonnull void (^)(NSString * _Nullable))completionHandler;

@end

NS_ASSUME_NONNULL_END
