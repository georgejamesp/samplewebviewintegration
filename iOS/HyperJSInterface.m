//
//  HyperJSInterface.m
//
//  Copyright © Juspay Technologies. All rights reserved.
//

#import "HyperJSInterface.h"
#import <WebKit/WebKit.h>
#import <HyperSDK/HyperSDK.h>

@interface HyperJSInterface()

@property (nonatomic, strong) NSDictionary *jsInterfaces;
@property (nonatomic, strong) HyperServices *hyperInstance;

@end

@implementation HyperJSInterface

- (id)initWithBaseViewController:(UIViewController *)viewController webView:(WKWebView *)webView {
    self = [super init];
    if (self) {
        self.baseViewController = viewController;
        self.webView = webView;
        self.jsInterfaces = @{ @"HyperServices" : self };
        self.hyperInstance = [HyperServices new];
    }
    
    return self;
}

- (void)addHyperServicesInterface {
    
    NSString *interfaceScript = @"window.HyperServices={};HyperServices.preFetch=function(e){var t=JSON.stringify({methodName:'preFetch:',returnType:'void',parameters:[{type:'String',value:e}]});return r=JSON.parse(prompt(t,'')),r.result},HyperServices.initiate=function(e){var t=JSON.stringify({methodName:'initiate:',returnType:'void',parameters:[{type:'String',value:e}]});return r=JSON.parse(prompt(t,'')),r.result},HyperServices.process=function(e){var t=JSON.stringify({methodName:'process:',returnType:'void',parameters:[{type:'String',value:e}]});return r=JSON.parse(prompt(t,'')),r.result},HyperServices.isInitialised=function(){var e=JSON.stringify({methodName:'isInitialised',returnType:'String',parameters:[]});return r=JSON.parse(prompt(e,'')),r.result},HyperServices.terminate=function(){var e=JSON.stringify({methodName:'terminate',returnType:'void',parameters:[]});return r=JSON.parse(prompt(e,'')),r.result};";
    
    [self.webView evaluateJavaScript:interfaceScript completionHandler:nil];
}

- (id)dictionaryFromString:(NSString*)string {
    
    if (string.length < 1) {
        return @{};
    }
    
    NSError *error;
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    if (error) {}
    return json;
}

- (NSString *)stringFromDictionary:(id)dict {
    
    if (!dict || ![NSJSONSerialization isValidJSONObject:dict]) {
        return @"";
    }
    
    NSString *data = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dict options:0 error:nil] encoding:NSUTF8StringEncoding];
    return data;
}

#pragma mark - HyperServices APIs

- (void)preFetch:(NSString *)stringPayload {
    NSDictionary *payload = [self dictionaryFromString:stringPayload];
    [HyperServices preFetch:payload];
}

- (void)initiate:(NSString *)stringPayload {
    NSDictionary *payload = [self dictionaryFromString:stringPayload];
    [self.hyperInstance initiate:self.baseViewController payload:payload callback:^(NSDictionary<NSString *,id> * _Nullable data) {
        if (data) {
            NSString *responseString = [self stringFromDictionary:data];
            NSString *callbackScript = [NSString stringWithFormat:@"window.onPaymentResponse('%@')", responseString];
            [self.webView evaluateJavaScript:callbackScript completionHandler:^(id _Nullable result, NSError * _Nullable error) {
                if (error) {
                    NSLog(@"Error while calling window.onPaymentResponse: %@", error.description);
                }
            }];
        }
    }];
}

- (void)process:(NSString *)stringPayload {
    NSDictionary *payload = [self dictionaryFromString:stringPayload];
    [self.hyperInstance process:payload];
}

- (void)terminate {
    [self.hyperInstance terminate];
}

- (NSString *)isInitialised {
    return [self.hyperInstance isInitialised] ? @"true" : @"false";
}

#pragma mark - WKUIDelegate runJavaScriptTextInputPanelWithPrompt

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(nonnull NSString *)prompt defaultText:(nullable NSString *)defaultText initiatedByFrame:(nonnull WKFrameInfo *)frame completionHandler:(nonnull void (^)(NSString * _Nullable))completionHandler {
    
    if ([prompt rangeOfString:@"methodName" options:NSCaseInsensitiveSearch].location!=NSNotFound) {
        NSError *error;
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:[prompt dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
        
        if (!error) {
            NSInvocation *invocation = nil;
            
            Boolean methodFound = false;
            
            for (NSString *key in self.jsInterfaces) {
                
                id jsInterface = [self.jsInterfaces valueForKey:key];
                if ([jsInterface respondsToSelector:NSSelectorFromString(data[@"methodName"])]) {
                    invocation = [NSInvocation invocationWithMethodSignature:[[jsInterface class] instanceMethodSignatureForSelector:NSSelectorFromString(data[@"methodName"])]];
                    [invocation setTarget:jsInterface];
                    methodFound = true;
                    break;
                }
            }
            
            if (!methodFound) {
                completionHandler(@"");
                return;
            }
            
            [invocation setSelector:NSSelectorFromString(data[@"methodName"])];
            
            int index=2;
            for (NSDictionary *parameter in data[@"parameters"]) {
                id value = parameter[@"value"];
                [invocation setArgument:&value atIndex:index];
                index++;
            }
            [invocation invoke];
            if ([data[@"returnType"] isEqualToString:@"String"]) {
                NSString * __unsafe_unretained resultA;
                [invocation getReturnValue:&resultA];
                if (resultA.length<1) {
                    resultA = @"";
                }
                NSDictionary *jsonDict = @{@"result":resultA};
                NSString *result = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding];
                completionHandler(result);
            } else if ([data[@"returnType"] isEqualToString:@"Boolean"]) {
                Boolean result;
                [invocation getReturnValue:&result];
                NSDictionary *jsonDict = @{@"result":[NSString stringWithFormat:@"%d",result]};
                NSString *resultReturn = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding];
                completionHandler(resultReturn);
            } else {
                completionHandler(@"{}");
            }
        } else {
            completionHandler(@"{}");
        }
    } else {
        completionHandler(@"");
    }
}

@end
