//
//  SampleViewController.m
//
//  Copyright © Juspay Technologies. All rights reserved.
//

#import "SampleViewController.h"
#import <WebKit/WebKit.h>

// Step 1
#import "HyperJSInterface.h"

@interface SampleViewController ()<WKUIDelegate, WKNavigationDelegate>

@property (nonatomic, strong) WKWebView *webView;

// Step 2
@property (nonatomic, strong) HyperJSInterface *hyperInteface;

@end

@implementation SampleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *indexURL = [[NSBundle mainBundle] URLForResource:@"index" withExtension:@"html"];
    self.webView = [[WKWebView alloc] initWithFrame:self.view.bounds];
    
    // Step 3
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;
    
    [self.webView loadFileURL:indexURL allowingReadAccessToURL:indexURL];
    
    // Step 4
    self.hyperInteface = [[HyperJSInterface alloc] initWithBaseViewController:self webView:self.webView];
    
    [self.view addSubview:self.webView];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    // Step 5
    [self.hyperInteface addHyperServicesInterface];
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler {
    
    // Step 6
    if ([prompt rangeOfString:@"methodName"].location != NSNotFound) {
        [self.hyperInteface webView:webView runJavaScriptTextInputPanelWithPrompt:prompt defaultText:defaultText initiatedByFrame:frame completionHandler:completionHandler];
    } else {
        // Do something else
    }
}

@end
